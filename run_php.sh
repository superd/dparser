#!/bin/bash
IFS=$'\n'

d=/home/superd/dcardpic
db=$d/db.txt
todo=$d/todo.txt
PIC=$d/pic.txt
ALLPIC=$d/allpic.txt
TITLE=$d/title.txt

# get list
echo "get list"
for i in `/usr/bin/php $d/get_list.php`
do
  url=`echo $i | cut -d',' -f1`
  title=`echo $i | cut -d',' -f2`
  gender=`echo $i | cut -d',' -f3`

  echo ---process $i

  if grep -q $url $db
  then
    echo ---duplicate $url
  else
    echo ------new $url

    if $(echo $title | grep -q 圖) 
    then
      if $(echo $gender | grep -q female)
      then
        echo $url >> $todo
      fi
    fi
    echo $url >> $db
  fi
done

# get article
echo "get article"
for i in `cat $todo`
do
  echo ---fetch $i
  OUT="$(mktemp /tmp/dcard-XXXX)"
  /usr/bin/php $d/get_detail.php $i | grep -v cloudfront| grep -v facebook.com > $OUT

  # get pic link
  grep imgur $OUT | sed -n 's/.*\(https:\/\/imgur\.dcard.tw\/\w\+\.jpg\).*/\1/p' >> $PIC

  # get pic
  for i in `cat $PIC`
  do
    echo get pic
    wget -N --directory-prefix=/home/superd/dcardpic/pic $i
  done

  # upload pic to flickr
  /home/superd/dcardpic/pic/uploadr.py

  # push all pic to TG
  /home/superd/telegram-bot/push-pic.sh

  # push full article to TG
  $d/push-full-article.sh $OUT
  mv $OUT /home/superd/dcardpic/allarticle/

  cp /dev/null $PIC
done

#echo "post pic only to slack"
#for i in `cat $PIC`
#do
#  /usr/bin/php $d/push_single.php $i
#done

# download all photos
#for i in `grep ^http $PIC`
#do
#  wget -N --directory-prefix=/home/superd/dcardpic/pic $i
#done

# post to telegram
#/home/superd/telegram-bot/push-pic.sh

#cp $PIC /tmp/pic.old
#grep ^http $PIC >> $ALLPIC
#grep -v ^http $PIC >> $TITLE

# post title to telegram
#echo "post title to telegram"
#/home/superd/telegram-bot/push-txt.sh

cp /dev/null $TITLE
cp /dev/null $PIC

#echo "post full article to slack and telegram"
#for i in `ls /tmp/dcard*`
#do
#  /usr/bin/php $d/push.php $i
#  $d/push-full-article.sh $i
#  mv $i /home/superd/dcardpic/allarticle/
#done

/bin/cp -f /dev/null $todo

