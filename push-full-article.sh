#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: " $0 "file"
  exit
fi

#CHAN='bottest'
CHAN='ENLTW-NSFW'

TG=/home/superd/telegram-bot/tg
# (echo "contact_list"; sleep 5; echo "msg bottest message")  | bin/telegram-cli -k tg-server.pub -l 1 -W

echo $1
(echo "contact_list"; sleep 5; echo "send_text $CHAN $1")  | $TG/bin/telegram-cli -k $TG/tg-server.pub -l 1 -W
#(echo "contact_list"; sleep 5; echo "msg debug $i")  | $TG/bin/telegram-cli -k $TG/tg-server.pub -l 1 -W
