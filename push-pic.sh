#!/bin/bash

PIC=/home/superd/dcardpic/pic
ARCH=/home/superd/dcardpic/pic/archive/

TG=/home/superd/telegram-bot/tg
# (echo "contact_list"; sleep 5; echo "msg bottest message")  | bin/telegram-cli -k tg-server.pub -l 1 -W
CHAN='ENLTW-NSFW'
#CHAN='bottest'

for i in `ls $PIC/*.png`
do
  echo $i
  (echo "contact_list"; sleep 5; echo "send_photo $CHAN $i")  | $TG/bin/telegram-cli -k $TG/tg-server.pub -l 1 -W
  (echo "contact_list"; sleep 5; echo "send_photo debug $i")  | $TG/bin/telegram-cli -k $TG/tg-server.pub -l 1 -W
  mv -f $i $ARCH
done
