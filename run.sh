#!/bin/bash
IFS=$'\n'

d=/home/superd/dcardpic
db=$d/db.txt
todo=$d/todo.txt
PIC=$d/pic.txt
ALLPIC=$d/allpic.txt
TITLE=$d/title.txt

# get list
echo "get list"
for i in `/usr/bin/phantomjs --ssl-protocol=tlsv1 $d/get.js | grep dcard | grep -v google`
do
  url=`echo $i | cut -d',' -f1`
  title=`echo $i | cut -d',' -f2`
  gender=`echo $i | cut -d',' -f3`

  echo ---process $i

  if grep -q $url $db
  then
    echo ---duplicate $url
  else
    echo ------new $url

    if $(echo $title | grep -q 圖) 
    then
      if $(echo $gender | grep -q F)
      then
        echo $url >> $todo
      fi
    fi
    echo $url >> $db
  fi
done

# get details
echo "get details"
for i in `cat $todo`
do
  echo ---fetch $i
  OUT="$(mktemp /tmp/dcard-XXXX)"
  /usr/bin/phantomjs --ssl-protocol=tlsv1 $d/get_detail.js $i | grep -A 99999 '=start=' > $OUT
  sed -i -e '/\/imgur/ s/$/\.jpg/' -e '/\/imgur/ s/\/imgur/\/i.imgur/' $OUT

  grep info $OUT >> $PIC
  grep imgur $OUT >> $PIC
  sed -i -e '/\/imgur/ s/$/\.jpg/' -e '/\/imgur/ s/\/imgur/\/i.imgur/' $PIC
done

echo "post pic only to slack"
for i in `cat $PIC`
do
  /usr/bin/php $d/push_single.php $i
  /usr/bin/php $d/push_single_ctlk.php $i
  /usr/bin/php $d/push_single_joehorn.php $i
done

# download all photos
for i in `grep ^http $PIC`
do
  wget --directory-prefix=/home/superd/dcardpic/pic $i
done

# post to telegram
/home/superd/telegram-bot/push-pic.sh

cp $PIC /tmp/pic.old
grep ^http $PIC >> $ALLPIC
grep -v ^http $PIC >> $TITLE

# post title to telegram
echo "post title to telegram"
/home/superd/telegram-bot/push-txt.sh

cp /dev/null $TITLE
cp /dev/null $PIC

echo "post full article to slack and telegram"
for i in `ls /tmp/dcard*`
do
  /usr/bin/php $d/push.php $i
  $d/push-full-article.sh $i
  mv $i /home/superd/dcardpic/allarticle/
done

/bin/cp -f /dev/null $todo

